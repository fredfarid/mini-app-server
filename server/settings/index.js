'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _dev = require('./dev.json');

var _dev2 = _interopRequireDefault(_dev);

var _prod = require('./prod.json');

var _prod2 = _interopRequireDefault(_prod);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let settings;
process.env.NODE_ENV === 'prod' ? settings = _prod2.default : settings = _dev2.default;

exports.default = settings;