'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.bootWebsocket = bootWebsocket;
exports.getWss = getWss;

var _expressWs = require('express-ws');

var _expressWs2 = _interopRequireDefault(_expressWs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

let wsInstance;

function bootWebsocket(app) {

    wsInstance = (0, _expressWs2.default)(app);
}

function getWss() {

    return wsInstance.getWss();
}