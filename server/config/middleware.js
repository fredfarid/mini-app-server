'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.useMiddlewaresPre = useMiddlewaresPre;
exports.useMiddlewaresPost = useMiddlewaresPost;

var _express = require('express');

var _serveFavicon = require('serve-favicon');

var _serveFavicon2 = _interopRequireDefault(_serveFavicon);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _compression = require('compression');

var _compression2 = _interopRequireDefault(_compression);

var _path = require('path');

var _path2 = _interopRequireDefault(_path);

var _settings = require('./../../server/settings');

var _settings2 = _interopRequireDefault(_settings);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// import xmlparser from 'express-xml-bodyparser';
function useMiddlewaresPre(app) {

    app.set('port', process.env.PORT || _settings2.default.port);
    app.use((0, _compression2.default)());
    app.use(_bodyParser2.default.json({ limit: '10mb' }));
    // app.use(xmlparser({
    //     explicitArray: false,
    //     normalizeTags: false
    // }));
    app.use(_bodyParser2.default.urlencoded({ limit: '10mb', extended: true }));
    app.use((0, _morgan2.default)('tiny'));
    app.use((0, _serveFavicon2.default)(_path2.default.resolve(__dirname, '..', '..', 'favicon.ico')));
    app.use("/image", (0, _express.static)(_path2.default.resolve(__dirname, '..', '..', 'image')));
    app.use("/js", (0, _express.static)(_path2.default.resolve(__dirname, '..', '..', 'js')));
    app.listen(app.get('port'), function () {
        console.log('express started.');
    });
}

function useMiddlewaresPost(app) {

    app.get('/', function (req, res) {

        res.sendFile(_path2.default.resolve(__dirname, '..', '..', 'index.html'));
    });

    app.get('/404', function (req, res) {

        res.sendFile(_path2.default.resolve(__dirname, '..', '..', '404.html'));
    });

    app.use(function (req, res) {

        res.redirect('/404');
    });

    app.use(function (err, req, res, next) {

        console.error(err.stack);
        res.type('text/plain');
        res.status(500);
        res.send('500 - Server Error');
    });
}