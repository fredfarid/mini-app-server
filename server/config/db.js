'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = connectDB;

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

var _settings = require('./../../server/settings');

var _settings2 = _interopRequireDefault(_settings);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function connectDB() {

    _mongoose2.default.Promise = global.Promise;
    _mongoose2.default.connect(_settings2.default.mongoDBUrl);
    _mongoose2.default.connection.once('open', function () {

        console.log('mongodb connected.');
    });
};