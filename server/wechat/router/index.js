"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = useUserRouters;

var _wechat = require("./wechat.router");

var _wechat2 = _interopRequireDefault(_wechat);

var _sign = require("./sign.router");

var _sign2 = _interopRequireDefault(_sign);

var _futureTelling = require("../listener/future-telling.listener");

var _futureTelling2 = _interopRequireDefault(_futureTelling);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function useUserRouters(app) {

    app.use('/wechat', _wechat2.default);
    app.use('/wechat/sign', _sign2.default);

    app.ws('/', _futureTelling2.default);
}