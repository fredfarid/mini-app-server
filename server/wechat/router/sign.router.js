'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

var _requestPromise = require('request-promise');

var _requestPromise2 = _interopRequireDefault(_requestPromise);

var _settings = require('./../../../server/settings');

var _settings2 = _interopRequireDefault(_settings);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = (0, _express.Router)();

router.get('/', function (req, res, next) {

    let name = req.query.name;
    console.log(name);
    let options = {
        url: _settings2.default.juhe.url,
        qs: {
            key: _settings2.default.juhe.key,
            consName: name,
            type: 'today'
        }
    };
    _requestPromise2.default.get(options).then(result => {
        console.log(result);
        res.json(result);
    });
});

router.post('/', function (req, res, next) {});

exports.default = router;