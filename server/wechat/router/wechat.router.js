'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _express = require('express');

const router = (0, _express.Router)();

router.get('/', function (req, res, next) {

    res.end('');
});

router.post('/', function (req, res, next) {});

exports.default = router;