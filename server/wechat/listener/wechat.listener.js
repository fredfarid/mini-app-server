'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function () {

    _io2.default.on('connection', socket => {

        console.log('one client connected.');
    });
};

var _io = require('./../../../server/config/io');

var _io2 = _interopRequireDefault(_io);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

;