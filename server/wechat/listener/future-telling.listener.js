'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

exports.default = function (ws, req) {

    ws.on('open', function (msg) {

        console.log('open: ' + msg);
    });

    ws.on('message', function (msg) {

        console.log(msg);
        (0, _websocket.getWss)().clients.forEach(function each(client) {

            // if (client !== ws)
            client.send(msg);
        });
    });

    ws.on('error', function (err) {

        console.log('err: ' + err);
    });

    ws.on('close', function (msg) {

        console.log('close: ' + msg);
    });
};

var _main = require('./../../../server/main');

var _websocket = require('./../../../server/config/websocket');