'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _mongoose = require('mongoose');

var _mongoose2 = _interopRequireDefault(_mongoose);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const schema = _mongoose2.default.Schema({
    name: String,
    password: String,
    email: String,
    mobile: String,
    // role: {type: Schema.Types.ObjectId, ref: 'Role'},
    createTime: { type: Date, default: Date.now },
    updateTime: { type: Date, default: Date.now }
});
const User = _mongoose2.default.model('User', schema);

exports.default = User;