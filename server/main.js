'use strict';

var _db = require('./../server/config/db.js');

var _db2 = _interopRequireDefault(_db);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _middleware = require('./../server/config/middleware');

var _websocket = require('./../server/config/websocket');

var _router = require('./../server/wechat/router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

(0, _db2.default)();

let app = (0, _express2.default)();

(0, _websocket.bootWebsocket)(app);

(0, _middleware.useMiddlewaresPre)(app);

(0, _router2.default)(app);

(0, _middleware.useMiddlewaresPost)(app);