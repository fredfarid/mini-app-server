import expressWs from 'express-ws';


let wsInstance;

export function bootWebsocket(app) {

    wsInstance = expressWs(app);
}

export function getWss() {

    return wsInstance.getWss();
}