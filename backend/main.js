import connectDB from "~/config/db.js";
import express from 'express';
import {useMiddlewaresPre, useMiddlewaresPost} from '~/config/middleware';
import {bootWebsocket} from '~/config/websocket';
import useWechatRouters from '~/wechat/router';


connectDB();

let app = express();

bootWebsocket(app);

useMiddlewaresPre(app);

useWechatRouters(app);

useMiddlewaresPost(app);





