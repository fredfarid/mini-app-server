import {Router} from 'express';
import request from 'request-promise';
import settings from '~/settings';

const router = Router();


router.get('/', function (req, res, next) {

    let name = req.query.name;
    console.log(name);
    let options = {
        url: settings.juhe.url,
        qs: {
            key:settings.juhe.key,
            consName: name,
            type:'today'
        }
    };
    request.get(options).then(result=> {
        console.log(result);
        res.json(result);
    });

});

router.post('/', function (req, res, next) {


});


export default router;
