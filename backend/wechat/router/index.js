import wechatRouter from "./wechat.router"
import signRouter from "./sign.router"
import futureTellingListener from "../listener/future-telling.listener"


export default function useUserRouters(app) {

    app.use('/wechat', wechatRouter);
    app.use('/wechat/sign', signRouter);

    app.ws('/', futureTellingListener);
}
