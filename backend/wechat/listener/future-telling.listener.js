import {wsInstance} from '~/main';
import {getWss} from '~/config/websocket';


export default function (ws, req) {

    ws.on('open', function (msg) {

        console.log('open: ' + msg);
    });

    ws.on('message', function (msg) {

        console.log(msg);
        getWss().clients.forEach(function each(client) {

            // if (client !== ws)
            client.send(msg);
        });
    });

    ws.on('error', function (err) {

        console.log('err: ' + err);
    });

    ws.on('close', function (msg) {

        console.log('close: ' + msg);
    });
}